{
  const {
    html,
  } = Polymer;
  /**
    `<component-tres>` Description.

    Example:

    ```html
    <component-tres></component-tres>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --component-tres | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class ComponentTres extends Polymer.Element {

    static get is() {
      return 'component-tres';
    }

    static get properties() {
      return {};
    }

    ready() {
      super.ready();
      console.log('Ejecutándose ready');
      this.$.pokemonDP.generateRequest();
    }

    _result(evt) {
      console.log('_result', evt);
      this.pokemones = evt.detail.results;
    }

    _indexMas(index){
      return parseInt(index) + 1;
    }

    static get template() {
      return html `
      <style include="component-tres-styles component-tres-shared-styles"></style>
      <slot></slot>
      
          <p>Call WS JSON</p>

          <cells-generic-dp
            id="pokemonDP"
            host="https://pokeapi.co"
            path="api/v2/pokemon"
            method="GET"
            on-request-success='_result'>
          </cells-generic-dp>

          <template is="dom-repeat" items="[[pokemones]]">
            <div>
              <h4>[[item.name]]</h4>
              <component-tres-hijo url="[[item.url]]"></component-tres-hijo>
            </div>
          </template>
      `;
    }
  }

  customElements.define(ComponentTres.is, ComponentTres);
}